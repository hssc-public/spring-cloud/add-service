package hu.dpc.edu.add;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AddController {

    @Value("${description}")
    private String description;
    @Value("${operation.name}")
    private String operationName;

    @GetMapping("/add/{num1:\\d+}/{num2:\\d+}")
    public long add(@PathVariable long num1, @PathVariable long num2) {
        return num1 + num2;
    }

    @GetMapping("/add.name")
    public String getOperationName() {
        return operationName;
    }
}
